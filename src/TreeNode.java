
import java.util.*;

public class TreeNode {

	// tegin mitmed meetodid selle näite pőhjal:
	// http://enos.itcollege.ee/~jpoial/algoritmid/TreeNode.java

	private String name;
	// esimene (järgmine) alluv
	private TreeNode firstChild;
	// parem (järgmine) naaber
	private TreeNode nextSibling;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		testName(name);
		this.name = name;
	}

	public TreeNode getFirstChild() {
		return firstChild;
	}

	public void setFirstChild(TreeNode firstChild) {
		this.firstChild = firstChild;
	}

	public TreeNode getNextSibling() {
		return nextSibling;
	}

	public void setNextSibling(TreeNode nextSibling) {
		this.nextSibling = nextSibling;
	}

	// konstruktor
	public TreeNode() {
	}

	// konstruktor
	TreeNode(String n) {
		testName(n);
		setName(n);
	}

	// konstruktor
	TreeNode(String n, TreeNode d, TreeNode r) {
		testName(n);
		setName(n);
		setNextSibling(d);
		setFirstChild(r);
	}

	// Kontrollib, kas tipu nimi on korrektne
	// tundub, et sellest meetodist ei ole midagi kasu, aga jätan igaksjuhuks
	public void testName(String name) {
		// Tipu nimi ei tohi olla tühi ega sisaldada ümarsulge, komasid ega
		// tühikuid
		if (name.isEmpty()) {
			throw new RuntimeException("Tipu nimi ei tohi olla tühi");
		}
		if (name.contains("(") || name.contains(")") || name.contains("}") || name.contains(",")
				|| name.contains(" ")) {
			throw new RuntimeException("Tipu nimi sisaldab keelatud tähemärki, tipu nimi: " + name);
		}
	}

	// meetod puu vasakpoolse suluesituse (String) järgi puu
	// moodustamiseks (parsePrefix, tulemuseks puu juurtipp)
	// nt "A(B1,C,D)"
	public static TreeNode parsePrefix(String s) {
		// kui string ei ole korrekte, viskab erindi
		testString(s);

		// kui ühetipuline puu
		if (testOneNode(s)) {
			return new TreeNode(s);
		}

		// true lőpus tähendab, et jätab sulud ja komad ka tokenitesse alles
		StringTokenizer stok = new StringTokenizer(s, "(),", true);
		TreeNode node = parseToken(stok);
		return node;
	}

	public static TreeNode parseToken(StringTokenizer stok) {
		TreeNode node = new TreeNode();

		while (stok.hasMoreTokens()) {
			String token = stok.nextToken();

			// kui sulu algus, siis järgneb alluv
			if (token.equals("(")) {
				node.addChild(parseToken(stok));

				// kui koma, siis järgneb naaber
			} else if (token.equals(",")) {
				node.addSibling(parseToken(stok));
				return node;

				// kui sulu lõpp, siis lõpetab selle haru
			} else if (token.equals(")")) {
				return node;

				// muul juhul on tegemist tipu nimega
			} else {
				node.setName(token);
			}
		}
		return node;
	}

	// tagastab suluesituse algusest juurtipu nime
	// see meetod pole enam vajalik
	public static String parseRootName(String suluesitus) {
		int i;
		for (i = 0; i < suluesitus.length() - 1; i++) {
			// kui jőuab suluni, siis enam ei ole nimi
			if (suluesitus.charAt(i) == '(') {
				break;
			}
		}
		return suluesitus.substring(0, i);
	}

	// kontrollib, kas etteantud string on korrektne, et sellest saaks puu
	// moodustada
	public static void testString(String suluesitus) {
		if (suluesitus.isEmpty() || suluesitus == null) {
			throw new RuntimeException("meetodile parsePrefix etteantav sőne on tühi");
		}

		// kontroll, kas sisaldab tühje sulge
		if (suluesitus.contains("()")) {
			throw new RuntimeException("meetodile parsePrefix etteantav sőne sisaldab tühje sulge: " + suluesitus);
		}

		// kontroll, kas sisaldab tühikuid vői tabe
		if (suluesitus.contains(" ") || suluesitus.contains("\t")) {
			throw new RuntimeException(
					"meetodile parsePrefix etteantav sőne sisaldab tühikuid vői tabe: " + suluesitus);
		}

		// kontroll, kas sulu avamisi ja sulgemisi on vőrdselt
		// lisaks kontrollib, kas koma on õige koha peal
		int suluAvamised = 0, suluSulgemised = 0;
		for (int i = 0; i < suluesitus.length(); i++) {
			if (suluesitus.charAt(i) == '(')
				suluAvamised += 1;
			if (suluesitus.charAt(i) == ')')
				suluSulgemised += 1;

			// kui tuleb kima
			if (suluesitus.charAt(i) == ',') {
				// koma saab olla siis, kui suluavamisi on rohkem kui sulgemisi
				if (suluAvamised <= suluSulgemised) {
					throw new RuntimeException(
							"meetodile parsePrefix etteantavas sőnes on koma vale koha peal: " + suluesitus);
				}
			}
		}
		// kui stringi lõpuks on avamisi ja sulgemisi erinev arv
		if (suluAvamised != suluSulgemised) {
			throw new RuntimeException("meetodile parsePrefix etteantavas sőnes on sulgude arv vale: " + suluesitus);
		}

		// kontroll, kas algab ikka őige sümboliga (vasakpoolne suluesitus ei
		// saa sulu ega komaga alata)
		if (suluesitus.startsWith("(") || suluesitus.startsWith(")") || suluesitus.startsWith(",")) {
			throw new RuntimeException(
					"meetodile parsePrefix etteantav sőne algab vale sümboliga: " + suluesitus.charAt(0));
		}

		// kontroll, et poleks kahte koma järjest
		if (suluesitus.contains(",,")) {
			throw new RuntimeException("meetodile parsePrefix etteantavas sõnes on kaks koma järjest " + suluesitus);
		}

		// kontroll, et poleks kahte avanevat sulgu järjest "(("
		// "))" - see on lubatud
		if (suluesitus.contains("((")) {
			throw new RuntimeException(
					"meetodile parsePrefix etteantavas sõnes on kaks avanevat sulgu järjest " + suluesitus);
		}

		// kontroll, et avanevale sulule ei järgneks koma
		// pärast ")" sulgu peaks olema koma lubatud
		if (suluesitus.contains("(,")) {
			throw new RuntimeException("meetodile parsePrefix etteantavas sőne on sulu järel koma: " + suluesitus);
		}
	}

	// kontrollib, kas tegemist vőib olla ühetipulise puuga (siis ei ole sulge)
	public static boolean testOneNode(String suluesitus) {
		if (suluesitus.contains("(")) {
			return false;
		}
		return true;
	}

	// meetod etteantud puu parempoolse suluesituse leidmiseks stringina
	// (puu juureks on tipp this)
	// A(B1,C,D) ==> (B1,C,D)A
	// +(*(-(5,1),7),/(6,3)) ==> (((5,1)-,7)*,(6,3)/)+
	public String rightParentheticRepresentation() {
		StringBuffer answer = new StringBuffer();

		// kui tipul on alluvaid ehk ta ei ole leht
		if (!isLeaf()) {
			answer.append("(");
			// rekursioon järgmise alluvaga
			answer.append(firstChild.rightParentheticRepresentation());
			answer.append(")");
		}
		answer.append(getName());

		// kui paremal on naaber
		if (hasNext()) {
			answer.append(",");
			// rekursioon järmise naabriga
			answer.append(nextSibling.rightParentheticRepresentation());
		}

		return answer.toString();
	}

	// kui paremal on naaber, tagastab true
	public boolean hasNext() {
		return (getNextSibling() != null);
	}

	// tagastab parempoole naabri
	public TreeNode next() {
		return getNextSibling();
	}

	// kui tipp on alluvateta, tagastab true
	public boolean isLeaf() {
		return (getFirstChild() == null);
	}

	// lisab alluva (kui alluvaid juba on, siis lisab alluvale kõige
	// parempoolsema naabri)
	public void addChild(TreeNode a) {
		// kui ühtegi alluvat ei ole
		if (isLeaf())
			// siis lisab alluva
			setFirstChild(a);

		// kui alluv on olemas
		else {
			// tipp selleks, et seda saaks kasutada järgmise naabri juurde
			// liikumiseks. (őppejőu näites oli kasutatud iteratorit)
			TreeNode children = getFirstChild();
			// tsükkel kestab seni, kuni paremal on naaber
			while (children.hasNext()) {
				// liigume next-iga ahela lőppu
				children = children.next();
			}
			// lisab uue naabri kőige parempoolseks (viimaseks)
			children.setNextSibling(a);
		}
	}

	// lisab naabri
	public void addSibling(TreeNode a) {
		// kui naabrit ei ole
		if (getNextSibling() == null) {
			// siis lisab naabri
			setNextSibling(a);
		}

		// kui naaber on olemas
		else {
			TreeNode sibling = getNextSibling();
			while (sibling.hasNext()) {
				sibling = sibling.next();
			}
			// lisab uue naabri kőige parempoolseks (viimaseks)
			sibling.setNextSibling(a);
		}
	}

	// arvutab puutippude arvu
	public int size() {
		int n = 1; // root
		TreeNode children = getFirstChild();
		while (children != null) {
			// pöördub iseenda poole (rekursiivne)
			n = n + children.size();
			children = children.next();
		}
		// kuna on rekursiivne, pole ülesviitasid vaja. returniga läheb tagasi
		// eelmisele tasemele
		return n;
	}

	// testimise jaoks. üks vőimalus, kuidas puud luua
	// viidastruktuur
	public static TreeNode createTree() {
		TreeNode root = new TreeNode("+", null,
				new TreeNode("*", new TreeNode("/", null, new TreeNode("6", new TreeNode("3", null, null), null)),
						new TreeNode("-", new TreeNode("4", null, null),
								new TreeNode("2", new TreeNode("1", null, null), null))));
		return root;
	}

	// testimise jaoks
	public static TreeNode createTree2() {
		TreeNode root = new TreeNode("1", null, null);
		return root;
	}

	public static void main(String[] param) {
		// TreeNode testPuu = createTree();
		// System.out.println("Number of nodes: " +
		// String.valueOf(testPuu.size()));
		// testPuu.addChild(new TreeNode("1", null, null));
		// System.out.println("Number of nodes: " +
		// String.valueOf(testPuu.size()));
		// testPuu.addChild(new TreeNode("2", null, null));
		// System.out.println("Number of nodes: " +
		// String.valueOf(testPuu.size()));
		// System.out.println(testPuu.getName());
		// System.out.println(testPuu.hasNext());

		String s = "A(B1,C,D)";
		// String s = "+(*(-(2,1),4),/(6,3))"; // "(((2,1)-,4)*,(6,3)/)+")
		TreeNode t = TreeNode.parsePrefix(s);
		// System.out.println(t.toString());
		String v = t.rightParentheticRepresentation();
		System.out.println(s + " ==> " + v); // A(B1,C,D) ==> (B1,C,D)A
	}
}